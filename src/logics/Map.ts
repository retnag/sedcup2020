import { Slot } from "./Slot";
import { Treasure } from "./Treasure";

export class Map {

    public slots: Slot[][] = [[],[]];

    public constructor(slotNum: number) {
        for (let i=0;i<slotNum;++i) {
            this.slots[0].push(new Slot(this, i, 0));
            this.slots[1].push(new Slot(this, i, 1));
        }
        for (let i=0;i<slotNum;++i) {
            while (Math.random() > 0.5) {
                let treasure;
                if (Math.random() > 0.4) {
                    treasure = new Treasure("Sack");
                } else {
                    treasure = new Treasure("Diamond");
                }
                this.slots[0][i].treasures.push(treasure);
            }
        }
        this.slots[0][slotNum - 1].treasures.push(new Treasure("Bag"));
    }

}