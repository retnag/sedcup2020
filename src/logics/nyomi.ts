import { Game } from "./Game";


let slotNum = 5;
let game = new Game(slotNum, ["Nyomi", "Puki", "Kaki"]);
game.gotSource("Nyomi", `function () {if (true) {
        return [
                { "type": "punch", "treasureType": "Sack", "direction": "right" },
        { "type": "shoot", "direction": "right" },
        { "type": "hmove" },
        { "type": "sheriffMove", "direction": "left" },
        { "type": "vmove", "count": 3, "direction": "right" },
        { "type": "vmove", "count": 3, "direction": "right" },
        { "type": "vmove", "count": 1, "direction": "right" },
        { "type": "hmove" },
        { "type": "loot", "treasureType": "Bag" },
    ]
} else {
        return [{ "type": "shoot" }]
}}`);
game.playIt();