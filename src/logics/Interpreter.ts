import { Game } from "./Game";
import { Player } from "./Player";

export class Interpreter {

    private game: Game;

    public constructor (game: Game) {
        this.game = game;
    }

    private forbidden = ["this"];

    validate(player: Player, source: string) {
        this.forbidden.forEach(f => {
            if (source.includes(f)) {
                return { type: "error", message: "The source code mustn't contain " + f + "!" };
            }
        });
        let actions = this.getActions(source);
        if (!Array.isArray(actions)) {
            return { type: "error", message: "Couldn't get the array from the source code" };
        }
        let playerActionNum = this.game.ticks - player.damage;
        let realActions = [];
        for (let i = 0, r = 0; i < this.game.ticks; ++i) {
            if (actions[i] === undefined || r >= playerActionNum) {
                realActions.push({ type: "nothing" });
                continue;
            }
            realActions.push(actions[i]);
            r++;
        }
        return { type: "success", actions: realActions };
    }

    getActions (source: string) {
        let actions = new Function("return " + source)();
        return actions();
    }

}
