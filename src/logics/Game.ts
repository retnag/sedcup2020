import { Map } from "./Map";
import { Interpreter } from "./Interpreter";
import { Player } from "./Player";
import { Slot } from "./Slot";
import { Treasure } from "./Treasure";
import { Sheriff } from "./Sheriff";

export class Game {

    private playerActions: any = {};

    private actionTypes = [
        "vmove", "hmove", "shoot", "loot", "sheriffMove", "nothing", "punch"
    ];

    public ticks: number = 10;
    public firstPlayerIndex = 1;
    public map: Map;
    private interpreter: Interpreter;
    private players: Player[] = [];
    private sheriff: Sheriff;

    public constructor (slotNum: number, playerNames: string[]) {
        this.map = new Map(slotNum);
        this.interpreter = new Interpreter(this);
        this.players = playerNames.map((name, i) => {
            let slot = this.map.slots[0][i%2%this.map.slots[0].length];
            return new Player(name, slot);
        });
        let lastSlot = this.map.slots[0][slotNum - 1];
        this.sheriff = new Sheriff("player4", lastSlot);
        // this.players.push(this.sheriff)

        for (let i=0;i<slotNum;++i) {
            console.log(this.map.slots[0][i]);
        }
        for (let i=0;i<slotNum;++i) {
            console.log(this.map.slots[1][i]);
        }
        
        console.log("---");
        console.log("---");
        console.log("---");
        console.log("---");
        

    }

    gotSource(playerName, source) {
        let player = this.players.filter(player => player.name === playerName)[0];
        let res = this.interpreter.validate(player, source);
        if (res.type === "error") {
            return false;
        }
        this.playerActions[playerName] = res.actions;
    }

    playIt() {
        this.players.forEach(player => {
            console.log(player.slot);
            
            if (this.playerActions[player.name] === undefined) {
                this.gotSource(player.name, "function () {return []}");
            }
        });
        for (let i = 0; i < this.ticks; ++i) {
            for (let j=0; j<this.players.length;++j) {
                let player = this.players[(j+this.firstPlayerIndex) % this.players.length];
                this.performAction(player, this.playerActions[player.name][i]);
            }
        }
        /*
        console.log(this.map.slots[0][0]);
        console.log(this.map.slots[0][1]);
        console.log(this.map.slots[0][2]);
        console.log(this.map.slots[0][3]);
        console.log(this.map.slots[0][4]);
        console.log(this.map.slots[1][0]);
        console.log(this.map.slots[1][1]);
        console.log(this.map.slots[1][2]);
        console.log(this.map.slots[1][3]);
        console.log(this.map.slots[1][4]);
        */
        // console.log(this.players);
        
    }

    performAction(player, action) { 
        if (typeof action !== 'object') {
            return;
        }
        if (this.actionTypes.indexOf(action.type)!== -1) {
            this[action.type](player, action);
        }
    }

    changeSlot(player: Player, slot: Slot) {
        // remove player from slot
        player.slot.players.splice(player.slot.players.indexOf(player), 1);
        // add player to new slot
        slot.players.push(player);
        // change player's slot
        player.slot = slot;
    }

    getLoot(player: Player, treasure: Treasure) {
        let index = player.slot.treasures.indexOf(treasure);
        console.log(index);
        
        if (index !== -1) {
            player.treasures.push(treasure);
            player.slot.treasures.splice(index, 1);
        }
    }

    loseLoot(player: Player, treasure: Treasure) {
        let index = player.treasures.indexOf(treasure);
        console.log(index);
        
        if (index !== -1) {
            player.slot.treasures.push(treasure);
            player.treasures.splice(index, 1);
        }
    }

    sheriffMeet(player) {
        if (player instanceof Sheriff) {
            return;
        }
        this.hmove(player, { "type": "hmove" });
        player.damage += 1;
    }

    vmove(player, action) {
        let diff = action.direction === 'left' ? -1 : 1;
        let count = action.count;
        if (player.slot.y === 0) {
            count = 1;
        } else {
            count = Math.min(3, count);
            count = Math.max(1, count);
        }

        let index = player.slot.x + (diff * count);
        index = Math.max(0, index);
        index = Math.min(this.map.slots[0].length - 1, index);
        
        let slot = this.map.slots[player.slot.y][index];

        this.changeSlot(player, slot);
        if (player.slot === this.sheriff.slot) {
            this.sheriffMeet(player);
        }
        console.log(player.name + " vmove");

        console.log(player.slot);
        
    }

    hmove(player, action) {
        let slot = this.map.slots[1-player.slot.y][player.slot.x];
        this.changeSlot(player, slot);
        if (player.slot === this.sheriff.slot) {
            this.sheriffMeet(player);
        }
        console.log(player.name + " hmove");

        console.log(player.slot);
    }

    shoot(player, action) {
        let direction = action.direction === 'left' ? -1 : 1;
        let count = player.slot.y === 0 ? 1 : this.map.slots[0].length;
        for (let i = 0, x = player.slot.x + direction; x >= 0 && x < this.map.slots[0].length && i < count; ++i, x += direction) {
            let slot = this.map.slots[player.slot.y][x];
            if (slot.players.length > 0) {
                let randI = Math.floor(Math.random() * slot.players.length);
                let other = slot.players[randI];
                other.damage += 1;
                player.shots += 1;
                console.log(other);
                break;
                
            }
        }
        console.log(player);
        
        console.log(player.name + " shoot");
    }

    loot(player, action) {
        var treasures = player.slot.treasures.filter(t => t.type === action.treasureType);
        console.log(player.slot.treasures);
        
        if (treasures.length > 0) {
            this.getLoot(player, treasures[0]);
        }
        console.log(player.name + " loot");
        console.log(player.treasures);
        console.log(player.slot.treasures);
    }

    punch(player, action) {
        console.log(player.slot.treasures);
        let others = player.slot.players.filter(p => p !== player);
        if (others.length > 0) {
            let other = others[0];
            if (action.treasureType !== undefined) {
                let treasures = other.treasures.filter(t => t.type === action.treasureType);
                if (treasures.length > 0) {
                    this.loseLoot(other, treasures[0]);
                    console.log("loose this");
                    console.log(treasures[0]);
                }
            }
            this.vmove(other, { type: 'vmove', direction: action.direction, count: 1 });
        }
        console.log(player.name + " punch");
        console.log(player.slot.treasures);
    }

    sheriffMove(player, action) {
        this.vmove(this.sheriff, { "type": "vmove", "direction": action.direction, "count": 1 });
        this.sheriff.slot.players.forEach(other => {
            this.sheriffMeet(other);
        });
        console.log(player.name + " sheriff");
    }

    nothing(player, action) {
        console.log(player.name + " nothing");
    }

}