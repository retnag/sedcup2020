import { Slot } from "./Slot";
import { Treasure } from "./Treasure";

export class Player {

    public name;
    public damage = 0;
    public shots = 0;
    public slot: Slot;
    public treasures: Treasure[] = [];

    public constructor(name, slot) {
        this.name = name;
        this.slot = slot;
        this.slot.players.push(this);
        this.treasures.push(new Treasure("Sack", 250));
        
    }

}
