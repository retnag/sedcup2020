export class Treasure {

    public static types = ["Diamond", "Bag", "Sack"];

    public type: string;
    public value: number;

    public constructor(type: string, value: number = undefined) {
        this.type = type;
        if (value === undefined) {
            if (type === "Diamond") {
                this.value = 500;
            } else if (type === "Bag") {
                this.value = 2000;
            } else {
                let rand = Math.floor(Math.random() * 6);
                this.value = 250 + 50 * rand;
            }
        } else {
            this.value = value;
        }
        
    }

}