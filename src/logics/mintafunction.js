function () {
  return [
    { "type": "punch", "treasureType": "Sack", "direction": "right" },
    { "type": "shoot", "direction": "right" },
    { "type": "hmove" },
    { "type": "sheriffMove", "direction": "left" },
    { "type": "vmove", "count": 3, "direction": "right" },
    { "type": "vmove", "count": 3, "direction": "right" },
    { "type": "vmove", "count": 1, "direction": "right" },
    { "type": "hmove" },
    { "type": "loot", "treasureType": "Bag" },
  ]
}
function () {
  return [
    { "type": "vmove", "count": 3, "direction": "right" },
    { "type": "hmove" },
    { "type": "vmove", "count": 2, "direction": "right" },
    { "type": "hmove" },
    { "type": "loot", "treasureType": "Bag" },
  ]
}
