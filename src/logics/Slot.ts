import { Map } from "./Map";
import { Player } from "./Player";
import { Treasure } from "./Treasure";

export class Slot {

    private map;

    private x;
    private y;
    public players: Player[] = [];
    public treasures: Treasure[] = [];

    public constructor(map: Map, x: number, y: number) {
        this.map = map;
        this.y = y;
        this.x = x;
    }

}
