/**
 * @author Manz Günter
 */
import * as React from "react";
import CodeMirror from 'react-codemirror';
import styled from "styled-components";

interface Props {
  p1f: (val: string) => void,
  p2f: (val: string) => void,
  p3f: (val: string) => void,

}
interface State {
  code: string

}

export default class Code extends React.Component<Props, State> {

  static defaultProps: Partial<Props> = {
  };

  constructor(props: Props) {
    super(props);
    this.state = {
      code: "type here"
    }
  }

  // handle window resize
  componentDidMount() { }
  componentWillUnmount() { }

  render() {
    const options = {
			lineNumbers: true,
    };

    return <div>
      <CodeMirror value={"function () {}"} onChange={this.props.p1f} options={options}></CodeMirror>
      <CodeMirror value={"function () {}"} onChange={this.props.p2f} options={options}></CodeMirror>
      <CodeMirror value={"function () {}"} onChange={this.props.p3f} options={options}></CodeMirror>
    </div> 
  }
}
