/**
 * @author Manz Günter
 */
import * as React from "react";
import "../../node_modules/codemirror/lib/codemirror.css";
import Code from "./Code";
import TrainGui from "./TrainGui";

export interface Props { }

export default class App extends React.Component<Props, {}> {
  static defaultProps: Partial<Props> = {
  };

  constructor(props: Props) {
    super(props);
    this.state = {
      code: "type here"
    }
  }

  // handle window resize
  componentDidMount() { }
  componentWillUnmount() { }

  render() {
    return <div>
      
      <TrainGui></TrainGui>
    </div>
  }
}