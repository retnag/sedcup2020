/**
 * @author Manz Günter
 */
import * as React from "react";

import bullet from '../res/bullet.png';
import boom from '../res/boom.png';
import player1 from '../res/player1.png';
import player2 from '../res/player2.png';
import player3 from '../res/player3.png';
import player4 from '../res/player4.png';
import bag from '../res/moneybag.png';
import diamond from '../res/diamond.png';
import aktataska from '../res/case.png';
import vagon from '../res/vagon.png';
import mozdony from '../res/mozdony.png';
import { Game } from "../logics/Game";
import Code from "./Code";


interface Props {

}
interface State {

}

const VAGON_SIZE = 400;


export default class TrainGui extends React.Component<Props, State> {

  static defaultProps: Partial<Props> = {
  };

  private gengine = null;

  constructor(props: Props) {
    super(props);

    this.game = new Game(3, ["player1", "player2", "player3"]);
    console.log(this.game.map.slots)
  }
  private game: Game;
  private canvas = React.createRef<HTMLCanvasElement>();
  private img = {
    vagon: React.createRef<HTMLImageElement>(),
    bullet: React.createRef<HTMLImageElement>(),
    boom: React.createRef<HTMLImageElement>(),
    seriff: React.createRef<HTMLImageElement>(),
    player1: React.createRef<HTMLImageElement>(),
    player2: React.createRef<HTMLImageElement>(),
    player3: React.createRef<HTMLImageElement>(),
    player4: React.createRef<HTMLImageElement>(),
    bag: React.createRef<HTMLImageElement>(),
    diamond: React.createRef<HTMLImageElement>(),
    aktataska: React.createRef<HTMLImageElement>(),
    mozdony: React.createRef<HTMLImageElement>(),
  }

  grender = () => {
    let ctx;
    if(this.canvas.current){
      ctx = this.canvas.current.getContext("2d");
      if(ctx){
        for (let i = 0; i < this.game.map.slots[0].length-1; i++) {
          let vagonX = (VAGON_SIZE+25) *i;
          let vagonY = 0;
          let fold = this.game.map.slots[0];
          let emelet = this.game.map.slots[1];
          //vagon
          ctx.drawImage(this.img.vagon.current, vagonX, vagonY, VAGON_SIZE, VAGON_SIZE);
          let tresSize = VAGON_SIZE/6;
          fold[i].treasures.forEach((treas, j) => {
            //treasure
            ctx.drawImage(this.maptresToImg(treas.type), vagonX + (j+1) * tresSize, vagonY + VAGON_SIZE/3, tresSize, tresSize);
          })
          emelet[i].treasures.forEach((treas, j) => {
            //treasure
            ctx.drawImage(this.maptresToImg(treas.type), vagonX + (j+1) * tresSize, vagonY + VAGON_SIZE*0.13, tresSize, tresSize);
          })
          
          
          tresSize = VAGON_SIZE/4;
          fold[i].players.forEach((player, j) => {
            ctx.drawImage(this.img[player.name].current, vagonX + (j+1) * (tresSize*0.7), vagonY + VAGON_SIZE/(1.8), tresSize, tresSize);
          })
          emelet[i].players.forEach((player, j) => {
            ctx.drawImage(this.img[player.name].current, vagonX + (j+1) * (tresSize*0.7), vagonY + VAGON_SIZE*0.08, tresSize, tresSize);
          })
          
        }
        
        //mozdony
        let i= this.game.map.slots[0].length-1
        let vagonX = (VAGON_SIZE+25) *i;
        let vagonY = 0;
        ctx.drawImage(this.img.mozdony.current, (VAGON_SIZE+25) *(i),0, VAGON_SIZE*1.4, VAGON_SIZE);
        let fold = this.game.map.slots[0];
        let emelet = this.game.map.slots[1];
        let tresSize = VAGON_SIZE/6;
        fold[i].treasures.forEach((treas, j) => {
          //treasure
          ctx.drawImage(this.maptresToImg(treas.type), vagonX + (j+1) * tresSize, vagonY + VAGON_SIZE/3, tresSize, tresSize);
        })

        fold[i].players.forEach((player, j) => {
          ctx.drawImage(this.img[player.name].current, vagonX + (j+1) * (tresSize*0.7), vagonY + VAGON_SIZE/(1.8), tresSize, tresSize);
        })
        emelet[i].players.forEach((player, j) => {
          ctx.drawImage(this.img[player.name].current, vagonX + (j+1) * (tresSize*0.7), vagonY + VAGON_SIZE*0.08, tresSize, tresSize);
        })
        // ctx.drawImage(this.img.player4.current, vagonX + (1) * (tresSize*0.7), vagonY + VAGON_SIZE/(1.8), tresSize, tresSize);
      }
    }

  }

  // handle window resize
  componentDidMount() {
    if(!this.gengine){
      this.gengine = setInterval(this.grender, 100)
    }
  }
  private p1code: string = "function () {}";
  private p2code: string = "function () {}";
  private p3code: string = "function () {}";
  p1changed= (val: string) => {
    this.p1code = val;
  }
  p2changed=(val: string) => {
    this.p2code = val;
  }
  p3changed= (val: string) => {
    this.p3code = val;
  }
  
  justdoit = () => {
    console.log(this.p1code)
    this.game.gotSource("player1", this.p1code);
    this.game.gotSource("player2", this.p2code);
    this.game.gotSource("player3", this.p3code);
    this.game.playIt();
  }
  render() {
    return <div>

        <Code 
          p1f={this.p1changed}
          p2f={this.p2changed}
          p3f={this.p3changed}
        ></Code>
        <button onClick={this.justdoit}>Play</button>
        <canvas ref={this.canvas} width={1800} height={600} />
        <img style={{display:"none"}} ref={this.img.vagon} src={vagon} className="hidden" />
        <img style={{display:"none"}} ref={this.img.mozdony} src={mozdony} className="hidden" />
        <img style={{display:"none"}} ref={this.img.bullet} src={bullet} className="hidden" />
        <img style={{display:"none"}} ref={this.img.boom} src={boom} className="hidden" />
        <img style={{display:"none"}} ref={this.img.player1} src={player1} className="hidden" />
        <img style={{display:"none"}} ref={this.img.player2} src={player2} className="hidden" />
        <img style={{display:"none"}} ref={this.img.player3} src={player3} className="hidden" />
        <img style={{display:"none"}} ref={this.img.player4} src={player4} className="hidden" />
        <img style={{display:"none"}} ref={this.img.bag} src={bag} className="hidden" />
        <img style={{display:"none"}} ref={this.img.diamond} src={diamond} className="hidden" />
        <img style={{display:"none"}} ref={this.img.aktataska} src={aktataska} className="hidden" />
       
    </div>
  }

  maptresToImg = (type: string): HTMLImageElement => {
    switch(type){
      case "Diamond": return this.img.diamond.current;
      case "Bag": return this.img.aktataska.current;
      case "Sack": return this.img.bag.current;
      default: return this.img.bullet.current;
    }
  }
}
